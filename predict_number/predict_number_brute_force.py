def predict_number(number):
    seq = [0]
    res = 0

    while len(seq) < number:
        seq_new = []
        for i in seq:
            if i == 0:
                seq_new.append(1)
            if i == 1:
                seq_new.append(2)
            if i == 2:
                seq_new.append(0)

        seq.extend(seq_new)

    res = seq[number]

    return res

if __name__ == "__main__":
    value = predict_number(300)
    print (value)
