from math import ceil, log2


def calculate_number_of_adds(digit):

    if (digit == 0):
        return 0

    # The value such that 2**power_of_2 > digit
    min_power_of_2 = ceil(log2(digit))

    if (digit == 2**(min_power_of_2)):
        return 1

    return 1 + predict_number(digit - 2**(min_power_of_2 - 1))


def predict_number(digit):
    return calculate_number_of_adds(digit) % 3


def build_seq(iteration):
    seq = [0]

    for i in range(iteration):
        seq_place_holder = seq.copy()
        for i in range(len(seq_place_holder)):
            seq_place_holder[i] = seq_place_holder[i] + 1

        seq = seq + seq_place_holder

    output = []

    for el in seq:
        output.append(el % 3)

    return output


if __name__ == "__main__":
    number = 456
    value = predict_number(number)

    for i, el in enumerate(build_seq(10)):
        cal = predict_number(i)
        exp = el

        if (cal != exp):
            print(f"i is: {i}, calculated: {cal}, expected: {exp}")
            break
        else:
            print(f"Correct for i equal {i}, calcualted: {cal}, expected: {exp}")

    print("check complete")

    number = 63
    print(predict_number(number))
